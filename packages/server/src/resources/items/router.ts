const { Router } = require('express');

const { getItemsTags, setItemTags } = require('./controllers');

const router = Router();

router.get('/', getItemsTags);
router.put('/:id', setItemTags);

module.exports = router;
