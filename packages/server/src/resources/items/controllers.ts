import { Request, Response } from 'express';
const db = require('../../db/connection');

const getItemsTags = (req: Request, res: Response) => {
  const itemsTags = db.get('itemsTags');
  res.status(200).json({ data: itemsTags });
};

const setItemTags = (req: Request, res: Response) => {
  const { id } = req.params;
  const { tags } = req.body;

  const itemsTagsDict = db.get('itemsTags');
  db.set('itemsTags', { ...itemsTagsDict, [id]: tags });

  res.status(200).json({ data: tags });
};

module.exports = { getItemsTags, setItemTags };
