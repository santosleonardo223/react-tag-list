const JSONdb = require('simple-json-db');

console.log('Connecting local DB');
const db = new JSONdb('./db.json');

module.exports = db;
