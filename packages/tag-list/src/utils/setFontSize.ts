import { css } from 'styled-components';

import theme from '../theme';

type FontSizes = keyof Pick<
  typeof theme.font,
  'sizeA' | 'sizeB' | 'sizeC' | 'sizeD'
>;

const setFontSize = (size: FontSizes) => {
  return css`
    font-size: ${theme.font[size].fontSize};
    line-height: ${theme.font[size].lineHeight};
  `;
};

export { setFontSize };
