const MAX_TAGS = 5;
const SEARCH_MIN_CHARACTERS = 3;

const MOCK_ITEMS_URL = 'https://my.api.mockaroo.com/movies.json?key=bf3c1c60';
const SERVER_URL = 'http://localhost:4000';

export { MAX_TAGS, SEARCH_MIN_CHARACTERS, MOCK_ITEMS_URL, SERVER_URL };
