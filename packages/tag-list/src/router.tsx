import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import ItemsPage from './pages/items';
import GenericPage from './pages/genericPage';

const Router = (): JSX.Element => {
  const { t } = useTranslation();

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={() => <ItemsPage />} />
        <Route
          path="/somethingWentWrong"
          component={() => <GenericPage title={t('somethingWentWrong')} />}
        />
        <Route
          path="*"
          component={() => <GenericPage title={t('notFound')} />}
        />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
