export type InputType =
  | 'checkbox'
  | 'color'
  | 'date'
  | 'email'
  | 'file'
  | 'number'
  | 'password'
  | 'radio'
  | 'text';

export type Item = {
  id: string;
  name: string;
  created_at: Date;
  tags: string[];
};
