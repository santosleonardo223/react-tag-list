import styled from 'styled-components';

import theme from '../../theme';
import { setFontSize } from '../../utils/setFontSize';

const SearchContainer = styled.div`
  display: flex;
  border: 1px solid ${({ theme }) => theme.colors.midGray};
  border-radius: ${({ theme }) => theme.radius.default};
  background-color: ${({ theme }) => theme.colors.white};
  overflow: hidden;

  svg {
    margin: auto 0;
    padding: ${({ theme }) => theme.spacing.default};
    fill: ${({ theme }) => theme.colors.darkGray};

    @media screen and (min-width: ${theme.breakpoints.md}) {
      padding: ${({ theme }) => theme.spacing.xSmall};
    }
  }

  input {
    flex: 1;
    border: none;
    padding: ${({ theme: { spacing } }) =>
      `${spacing.small} ${spacing.default}`};
    ${setFontSize('sizeD')}

    @media screen and (min-width: ${theme.breakpoints.md}) {
      ${setFontSize('sizeC')}
    }

    :focus {
      outline: none;
    }
  }
`;

const StyledInput = styled.input``;

export { SearchContainer, StyledInput };
