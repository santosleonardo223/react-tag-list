import React from 'react';

import { InputType } from '../types';
import { SearchContainer, StyledInput } from './styles';
import { Search } from '../../icons/Search';

type SearchProps = {
  type?: InputType;
  value: string | number;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  placeholder?: string;
};

const SearchBar = ({
  type = 'text',
  value,
  onChange,
  placeholder,
}: SearchProps): JSX.Element => {
  return (
    <SearchContainer>
      <Search />
      <StyledInput
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
      />
    </SearchContainer>
  );
};

export default SearchBar;
