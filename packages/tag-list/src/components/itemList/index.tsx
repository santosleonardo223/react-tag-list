import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { Item } from '../types';
import {
  AddTagsContainer,
  ItemListContainer,
  NameContainer,
  TagsContainer,
} from './styles';
import { MAX_TAGS, SERVER_URL } from '../../constants';
import Tag from '../tag';

type ItemListProps = {
  item: Item;
  handleItemUpdate: (newItem: Item) => void;
};

const ItemList = ({ item, handleItemUpdate }: ItemListProps): JSX.Element => {
  const { t } = useTranslation();
  const history = useHistory();
  const [newTag, setNewTag] = useState('');

  const handleOnChangeNewTag = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewTag(e.target.value);
  };

  const updateItem = async (tags: string[]) => {
    try {
      await fetch(`${SERVER_URL}/items/${item.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ tags }),
      }).then((res) => res.json());

      handleItemUpdate({
        ...item,
        tags: tags,
      });
      setNewTag('');
    } catch (err) {
      console.error(err);
      history.push('/somethingWentWrong');
    }
  };

  const handleNewTagKeyUp = async (
    e: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (e.key === 'Enter' && newTag.length) {
      const newTags = [...item.tags, newTag.trim()];
      await updateItem(newTags);
      setNewTag('');
    }
  };

  const addNewTag = () => {
    if (newTag) {
      const newTags = [...item.tags, newTag.trim()];
      updateItem(newTags);
    }
  };

  const handleRemoveTag = async (title: string) => {
    const newTags = item.tags.filter((tag) => tag !== title);

    try {
      await fetch(`${SERVER_URL}/items/${item.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ tags: newTags }),
      });

      handleItemUpdate({
        ...item,
        tags: [...newTags],
      });
    } catch (err) {
      console.error(err);
      history.push('/somethingWentWrong');
    }
  };

  const { id, name, created_at, tags } = item;

  return (
    <ItemListContainer>
      <NameContainer>
        <h2>{name}</h2>
        <p>{created_at}</p>
      </NameContainer>

      <TagsContainer>
        {tags.length
          ? tags.map((tag) => (
              <Tag
                key={`${id}_${tag}`}
                title={tag}
                handleClose={handleRemoveTag}
              />
            ))
          : null}
      </TagsContainer>

      <AddTagsContainer>
        <input
          type="text"
          placeholder={t(
            tags.length >= MAX_TAGS ? 'maxTagsReached' : 'typeTagName'
          )}
          value={newTag}
          onChange={handleOnChangeNewTag}
          onKeyUp={handleNewTagKeyUp}
          disabled={tags.length >= MAX_TAGS}
        />
        <button type="button" onClick={addNewTag} disabled={!newTag}>
          {t('addTag')}
        </button>
      </AddTagsContainer>
    </ItemListContainer>
  );
};

export default ItemList;
