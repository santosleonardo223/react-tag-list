import styled from 'styled-components';

import theme from '../../theme';
import { setFontSize } from '../../utils/setFontSize';

const ItemListContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(3, 1fr);
  gap: ${theme.spacing.small};

  background-color: ${theme.colors.midGray};
  margin-top: ${theme.spacing.default};
  padding: ${theme.spacing.small} ${theme.spacing.default};
  border-radius: ${theme.radius.default};

  @media screen and (min-width: ${theme.breakpoints.md}) {
    grid-template-columns: minmax(0, 1fr) minmax(0, 2fr) 1fr;
    grid-template-rows: 1fr;
  }
`;

const NameContainer = styled.div`
  display: flex;
  justify-content: space-between;

  h2 {
    ${setFontSize('sizeD')}
    margin: auto 0;
  }

  p {
    ${setFontSize('sizeB')}
    margin: auto 0;
  }

  @media screen and (min-width: ${theme.breakpoints.md}) {
    flex-direction: column;

    h2 {
      ${setFontSize('sizeC')}
    }
  }
`;

const TagsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(${theme.widths.maxTag}, 1fr));
  gap: ${theme.spacing.small};
  direction: rtl;
  align-items: center;
`;

const AddTagsContainer = styled.div`
  display: flex;
  justify-content: space-between;

  input {
    flex: 1;
    margin: auto 0;
    padding: ${theme.spacing.small} ${theme.spacing.default};
    border: 1px solid ${theme.colors.midGray};
    ${setFontSize('sizeC')}

    :focus {
      outline: 1px solid ${theme.colors.darkGray};
    }

    :disabled {
      background-color: ${theme.colors.midGray};

      ::placeholder {
        color: ${theme.colors.black};
      }
    }
  }

  button {
    white-space: nowrap;
    margin: auto ${theme.spacing.default};
    padding: 0 ${theme.spacing.medium};
    height: ${theme.heights.button};
    ${setFontSize('sizeC')}

    background-color: ${theme.colors.moss};
    color: ${theme.colors.white};
    border: none;
    border-radius: ${theme.radius.default};
    cursor: pointer;

    :disabled {
      background-color: ${theme.colors.darkGray};
      cursor: default;
    }
  }

  @media screen and (min-width: ${theme.breakpoints.md}) {
    input {
      padding: ${theme.spacing.xSmall} ${theme.spacing.default};
      ${setFontSize('sizeB')}
    }

    button {
      height: ${theme.heights.smallButton};
      ${setFontSize('sizeB')}
    }
  }
`;

export { ItemListContainer, NameContainer, TagsContainer, AddTagsContainer };
