import styled from 'styled-components';

import theme from '../../theme';

const TagContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: ${theme.spacing.xSmall} ${theme.spacing.default};
  background-color: ${theme.colors.blue};
  border-radius: ${theme.radius.tag};
  height: ${theme.heights.tag};
  direction: ltr;

  p {
    flex: 1;
    margin: auto 0;
    color: ${theme.colors.white};
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  svg {
    margin: auto 0 auto ${theme.spacing.default};
    fill: ${theme.colors.white};
    width: ${theme.widths.smallIcon};
    height: ${theme.heights.smallIcon};
    cursor: pointer;
  }
`;

export { TagContainer };
