import React from 'react';

import { TagContainer } from './styles';
import { Close } from '../../icons/Close';

type TagProps = {
  title: string;
  handleClose: (tagName: string) => void;
};

const Tag = ({ title, handleClose }: TagProps): JSX.Element => {
  return (
    <TagContainer>
      <p>{title}</p>
      <Close onClick={() => handleClose(title)} />
    </TagContainer>
  );
};

export default Tag;
