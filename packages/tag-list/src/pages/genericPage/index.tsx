import React from 'react';

type GenericPageProps = {
  title: string;
};

const GenericPage = ({ title }: GenericPageProps): JSX.Element => {
  return <h1>{title}</h1>;
};

export default GenericPage;
