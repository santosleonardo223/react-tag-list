import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { PageContainer, SearchContainer } from './styles';
import SearchBar from '../../components/search';
import { Item } from '../../components/types';
import ItemList from '../../components/itemList';
import {
  MOCK_ITEMS_URL,
  SEARCH_MIN_CHARACTERS,
  SERVER_URL,
} from '../../constants';

const ItemsPage = (): JSX.Element => {
  const { t } = useTranslation();
  const history = useHistory();
  const [search, setSearch] = useState('');
  const [items, setItems] = useState<Item[]>([]);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const handleItemUpdate = (updatedItem: Item) => {
    const index = items.findIndex((item) => item.id === updatedItem.id);
    if (index > -1) {
      const newItemList = [...items];
      newItemList[index] = updatedItem;
      setItems(newItemList);
    }
  };

  useEffect(() => {
    const fetchItemsAndTags = async () => {
      try {
        const items: Omit<Item, 'tags'>[] = await fetch(`${MOCK_ITEMS_URL}`, {
          method: 'GET',
        }).then((res) => res.json());

        const { data: itemsTagsDict }: { data: { [id: string]: string[] } } =
          await fetch(`${SERVER_URL}/items/`, {
            method: 'GET',
          }).then((res) => res.json());

        const itemsWithTags: Item[] = items.map((i) => ({
          ...i,
          tags: itemsTagsDict[i.id] ?? [],
        }));
        setItems(itemsWithTags);
      } catch (err) {
        console.error(err);
        history.push('/somethingWentWrong');
      }
    };

    fetchItemsAndTags();
  }, []);

  const filteredItems =
    search.length >= SEARCH_MIN_CHARACTERS
      ? items.filter((item) => {
          const lowerCasedTags = item.tags.map((tag) =>
            tag.toLocaleLowerCase()
          );
          return lowerCasedTags.includes(search.toLocaleLowerCase());
        })
      : items;

  return (
    <PageContainer>
      <SearchContainer>
        <SearchBar
          placeholder={t('searchTags')}
          value={search}
          onChange={handleSearch}
        />
      </SearchContainer>

      {filteredItems.length
        ? filteredItems.map((item) => (
            <ItemList
              key={item.id}
              item={item}
              handleItemUpdate={handleItemUpdate}
            />
          ))
        : null}
    </PageContainer>
  );
};

export default ItemsPage;
