import styled from 'styled-components';
import theme from '../../theme';

const PageContainer = styled.div`
  padding: ${theme.spacing.small};

  @media screen and (min-width: ${theme.breakpoints.md}) {
    padding: ${theme.spacing.default};
  }
`;

const SearchContainer = styled.div`
  width: 100%;

  @media screen and (min-width: ${theme.breakpoints.md}) {
    width: 33%;
    margin-left: auto;
  }
`;

export { PageContainer, SearchContainer };
