import React, { Fragment } from 'react';
import { ThemeProvider, createGlobalStyle } from 'styled-components';

import initializeI18Next from './localization/i18n';
import theme from './theme';
import Router from './router';

const GlobalStyles = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    
    font-family: ${theme.font.family};
    
    background-color: ${theme.colors.lightGray};
  }
`;

const App = (): JSX.Element => {
  initializeI18Next('en');

  return (
    <Fragment>
      <GlobalStyles />
      <ThemeProvider theme={theme}>
        <Router />
      </ThemeProvider>
    </Fragment>
  );
};

export default App;
