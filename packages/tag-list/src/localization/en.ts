const en = {
  translation: {
    // Labels
    notFound: 'Not found',
    searchTags: 'Search tags',
    somethingWentWrong: 'Something went wrong',
    typeTagName: 'Type tag name',
    maxTagsReached: 'Max tags reached',
    addTag: 'Add Tag',
  },
};

export default en;
