import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import en from './en';

type Locale = 'en';

const initializeI18Next = (locale: Locale) => {
  i18n.use(initReactI18next).init({
    resources: {
      en,
    },
    lng: locale,
    fallbackLng: 'en',

    interpolation: {
      escapeValue: false,
    },
  });
};

export default initializeI18Next;
