const theme = {
  colors: {
    black: '#000',
    blue: '#3E70AE',
    lightGray: '#F3F3F3',
    midGray: '#E8E8E8',
    darkGray: '#545454',
    moss: '#006966',
    red: '#FF0000',
    white: '#fff',
  },
  widths: {
    smallIcon: '14px',
    icon: '24px',
    maxTag: '100px',
  },
  heights: {
    smallIcon: '14px',
    icon: '24px',
    smallButton: '30px',
    button: '42px',
    tag: '24px',
  },
  font: {
    family: 'CoreSans, Helvetica, Arial, sans-serif',
    sizeA: { fontSize: '12px', lineHeight: '16px' },
    sizeB: { fontSize: '14px', lineHeight: '20px' },
    sizeC: { fontSize: '16px', lineHeight: '24px' },
    sizeD: { fontSize: '20px', lineHeight: '28px' },
    weight: {
      regular: 'normal',
      medium: '600',
      bold: 'bold',
    },
  },
  spacing: {
    xSmall: '4px',
    small: '8px',
    default: '16px',
    medium: '24px',
    large: '32px',
  },
  radius: {
    default: '4px',
    tag: '20px',
  },
  breakpoints: {
    xs: '0px',
    sm: '600px',
    md: '960px',
    lg: '1280px',
  },
};

export default theme;
