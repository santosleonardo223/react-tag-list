export type IconProps = {
  onClick?: () => void;
  width?: string;
  height?: string;
  fill?: string;
  stroke?: string;
};
