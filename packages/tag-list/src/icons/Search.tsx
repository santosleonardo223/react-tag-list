import React from 'react';

import theme from '../theme';
import { IconProps } from './types';

const Search = ({ onClick, width, height, fill }: IconProps): JSX.Element => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    onClick={onClick}
    width={width || theme.widths.icon}
    height={height || theme.widths.icon}
    fill={fill || 'inherit'}
  >
    <path d="M0,24H24V0H0Z" fill="none" />
    <path d="M19.717,18.275,15.422,13.98a6.949,6.949,0,1,0-1.417,1.412l4.3,4.3a1,1,0,0,0,1.414-1.415ZM4.917,9.82a4.945,4.945,0,1,1,4.944,4.944A4.95,4.95,0,0,1,4.917,9.82Z" />
  </svg>
);

export { Search };
