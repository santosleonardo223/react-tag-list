---============= DESCRIPTION =============---

This is a simple monorepo composed of a server and a frontend application. Both applications are inside the "./packages" folder.
The frontend project is under "./packages/tag-list". It will show a list of items that can hold tags. The user will be able to add/remove tags from the items.
The backend is a simple NodeJS + express under "./packages/server". It will run a mock database which, by your turn, is used by the frontend project.


---============= HOW TO RUN =============---

To run both the server and the frontend, run these two commands from project root folder:
	yarn
	yarn start

The "yarn start" script will build the frontend project and then run the server and client application.